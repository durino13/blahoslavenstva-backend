// Modules

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';

// Componetns

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LayoutComponent } from './pages/layout/layout.component';
import { MainHeaderComponent } from './pages/main-header/main-header.component';
import { MainSideComponent } from './pages/main-side/main-side.component';
import { ArticleListComponent } from './pages/article-list/article-list.component';
import { ArticleDetailComponent } from './pages/article-detail/article-detail.component';
import { IconComponent } from './component/icon/icon.component';
import { CheckboxComponent } from './component/checkbox/checkbox.component';
import { CKEditorComponent } from '../../node_modules/ng2-ckeditor/src/ckeditor.component';
import { CkeditorComponent } from './component/ckeditor/ckeditor.component';

// Services

import { ArticleService } from './services/article.service';
import { PositionService } from './services/position.service';
import { LoginComponent } from './pages/login/login.component';
import { CategoryService } from './services/category.service';

@NgModule({
    declarations: [
        AppComponent,
        LayoutComponent,
        MainHeaderComponent,
        MainSideComponent,
        ArticleListComponent,
        ArticleDetailComponent,
        IconComponent,
        CheckboxComponent,
        CKEditorComponent,
        CkeditorComponent,
        LoginComponent
    ],
    entryComponents: [
        IconComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        AppRoutingModule,
        Ng2SmartTableModule,
        FormsModule,
        NgSelectModule,
        HttpClientModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot()
    ],
    providers: [
        ArticleService,
        PositionService,
        CategoryService
    ],
    exports: [
        CKEditorComponent
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
