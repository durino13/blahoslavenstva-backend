import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Article } from '../model/article';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/Rx';

@Injectable()
export class ArticleService {

    // TODO Hardcoded URL, get the host from the config ..
    protected articlesUrl = 'http://blahoslavenstva-service.local.d:8071/api/articles';

    protected articles;

    constructor(private http: HttpClient) {
    }

    /**
     *
     * Cast service response from javascript object into a typescript class ..
     *
     * @param response
     * @returns {any}
     */
    castResponse(response){
        return new Article().deserialize(response);
    }

    /**
     *
     * Find article by ID
     *
     * @param {number} id
     * @returns {Observable<Article>}
     */
    getArticleById(id: number): Observable<Article> {
        const url = `${this.articlesUrl}/${id}`;
        return this.http.get<Article>(url).map((response) => this.castResponse(response));
    }

    /**
     *
     * Get all articles
     *
     * @returns {Observable<Article[]>}
     */
    getArticles(): Observable<Article[]> {
        return this.http.get<Article[]>(this.articlesUrl);
    }

    /**
     *
     * Create or update the article
     *
     * @param {number} id
     * @returns {any}
     */
    save(id: number, payload: number): any {
        const url = `${this.articlesUrl}/${id}`;

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json'
            })
        };

        return this.http.put(url, payload, httpOptions).subscribe();
    }

}
