import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Category } from '../model/category';


@Injectable()
export class CategoryService {

    // TODO Hardcoded URL
    private categoriesUrl = 'http://blahoslavenstva-service.local.d:8071/api/categories';

    private http: HttpClient;

    constructor(private injector: Injector) {
    }

    getCategories(): Observable<Category[]> {
        this.http = this.injector.get(HttpClient);
        return this.http.get<Category[]>(this.categoriesUrl);
    }

}
