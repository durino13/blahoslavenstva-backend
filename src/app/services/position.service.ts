import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ArticlePosition } from '../model/position';

@Injectable()
export class PositionService {

    private positionsUrl = 'http://blahoslavenstva-service.local.d:8071/api/positions';

    private http: HttpClient;

    constructor(private injector: Injector) {
    }

    getPositions(): Observable<ArticlePosition[]> {
        this.http = this.injector.get(HttpClient);
        return this.http.get<ArticlePosition[]>(this.positionsUrl);
    }

}
