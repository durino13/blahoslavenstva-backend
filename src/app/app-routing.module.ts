import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './pages/layout/layout.component';
import { ArticleListComponent } from './pages/article-list/article-list.component';
import { ArticleDetailComponent } from './pages/article-detail/article-detail.component';
import { LoginComponent } from './pages/login/login.component';


@NgModule({
    imports: [
        RouterModule.forRoot([
            // { path: '', redirectTo: '/operations', pathMatch: 'full'},
            { path: '', component: LayoutComponent, children: [
                    { path: 'articles', component: ArticleListComponent },
                    { path: 'articles/:article_id', component: ArticleDetailComponent },
                    { path: 'login', component: LoginComponent }
                ]},
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
