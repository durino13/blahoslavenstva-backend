import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { Article } from '../../model/article';
import { IconComponent } from '../../component/icon/icon.component';
import { Router } from '@angular/router';

@Component({
    selector: 'app-article-list',
    templateUrl: './article-list.component.html',
    styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

    private articles: Article[];

    settings = {
        actions: false,
        pager: {
            display: true,
            perPage: 30
        },
        columns: {
            'test': {
                title: 'Actions',
                width: '5%',
                filter: false,
                type: 'custom',
                renderComponent: IconComponent
            },
            article_id: {
                title: 'ID',
                width: '5%'
            },
            title: {
                title: 'Názov článku',
                width: '35%',
            },
            date_created: {
                title: 'Dátum vytvorenia článku',
                width: '10%',
            },
            published: {
                title: 'Publikovaný',
                width: '5%',
            }
        }
    };

    constructor(private articleService: ArticleService, private router: Router) {
    }

    ngOnInit() {
        this.getArticles();
    }

    /**
     * Get articles
     */
    getArticles(): void {
        this.articleService.getArticles()
            .subscribe(articles => { this.articles = articles; });
    }

    /**
    |--------------------------------------------------------------------------
    | Events
    |--------------------------------------------------------------------------
    |
    |
    */

    onUserRowSelect(event): void {
        this.router.navigateByUrl('articles/' + event.data.article_id);
    }

}
