import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

    isLoggedIn = true;
    bodyClasses = 'skin-blue sidebar-mini';

    body = document.getElementsByTagName('body')[0];

    constructor(private router: Router) {
        if (this.router.url === '/login') {
            this.isLoggedIn = false;
        }
    }

    ngOnInit() {
        // add the the body classes
        this.body.classList.add('skin-blue');
        this.body.classList.add('sidebar-mini');
    }

    // ngOnDestroy() {
    //     // remove the the body classes
    //     this.body.classList.remove('skin-blue');
    //     this.body.classList.remove('sidebar-mini');
    // }

}
