import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'admin-main-side',
    templateUrl: './main-side.component.html',
    styleUrls: ['./main-side.component.css']
})
export class MainSideComponent implements OnInit {

    @Input()
    isLoggedIn: boolean;
    config: object;

    constructor() {
        this.config = environment;
    }

    ngOnInit() {
    }

}
