import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { Article } from '../../model/article';
import { ActivatedRoute } from '@angular/router';
import { ArticlePosition } from '../../model/position';
import { PositionService } from '../../services/position.service';
import * as moment from 'moment';
import { Category } from '../../model/category';
import { CategoryService } from '../../services/category.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-article-detail',
    templateUrl: './article-detail.component.html',
    styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {

    protected show_event: boolean;

    protected article: Article;

    protected positions: ArticlePosition[];

    protected categories: Category[];

    constructor(private route: ActivatedRoute,
                private articleService: ArticleService,
                private positionService: PositionService,
                private categoryService: CategoryService,
                private toastr: ToastrService) {}

    ngOnInit() {

        this.show_event = false;

        // Load articles from the service

        this.route.params.subscribe(params => {
            const article_id = +params['article_id'];
            this.getArticleById(article_id);
        });

        // Load positions

        this.positionService.getPositions()
            .subscribe(positions => this.positions = positions);

        // Categories

        this.categoryService.getCategories()
            .subscribe(categories => this.categories = categories);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    |
    |
    */

    getArticleById(id: number) {
        this.articleService.getArticleById(id)
            .subscribe(article => {
                // Format the date
                article.date_created = moment(article.date_created).format('YYYY-M-D');
                this.show_event = article.isEvent();
                this.article = article;
            });
    }

    /*
    |--------------------------------------------------------------------------
    | Event handlers
    |--------------------------------------------------------------------------
    |
    |
    */

    onSubmit(payload: any) {
        const article_id = payload.article_id;
        this.articleService.save(article_id, payload);
        this.toastr.success('The article has been successfully saved!', 'Success');
    }

    onArticleHasEventStateChanged(show_event: boolean) {
        this.show_event = show_event;
    }

}
