import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-icon',
  template: `
      <a [routerLink]="link" [style.color]="style">
          <i class="fas fa-pencil-alt"></i>
      </a>
  `,
  styleUrls: ['./icon.component.css']
})
export class IconComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
