import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ValueAccessorBase } from './valueAccessorBase';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: CheckboxComponent, multi: true}
  ],
})
export class CheckboxComponent extends ValueAccessorBase<boolean> implements OnInit {

  @Input() label: string;
  @Input() name: string;
  @Input() checked: boolean;
  @Output() checkedChange = new EventEmitter();

  ngOnInit() {
  }

}
