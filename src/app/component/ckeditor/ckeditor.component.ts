import { Component, Input, OnInit, AfterViewChecked } from '@angular/core';

@Component({
    selector: 'app-ckeditor',
    templateUrl: './ckeditor.component.html',
    styleUrls: ['./ckeditor.component.css']
})
export class CkeditorComponent implements OnInit, AfterViewChecked {

    @Input()
    name: string;

    @Input()
    content: string;

    constructor() {
    }

    ngOnInit() {
    }

    ngAfterViewChecked() {
        // CKEDITOR.replace(this.name);
    }

}
