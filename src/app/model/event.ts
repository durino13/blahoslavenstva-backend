import { Category } from './category';

export interface ArticleEvent {
    id: number;
    category: Category;
    location: string;
    lecturer: string;
    event_date_from: string;
    event_date_until: string;
    event_start_time: string;
}
