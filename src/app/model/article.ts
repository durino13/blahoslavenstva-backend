import { ArticlePosition } from './position';
import { ArticleEvent } from './event';
import { Deserializable } from './deserializable';

export class Article implements Deserializable {

    article_id: number;
    title: string;
    intro_text: string;
    article_text: string;
    published: boolean;
    contains_event: boolean;
    date_created: string;
    positions: ArticlePosition[];
    event: ArticleEvent;

    /**
     *
     * Vytvorenie noveho objektu
     *
     * @param input
     * @returns {any}
     */
    public deserialize(input: any): Article {
        // TODO pozriet sa na object assign .. Co ked v input dostanem data, ktore nemam definovane v modeli?
        Object.assign(this, input);
        return this;
    }

    /**
     *
     * Determines, if the article contains info about an event
     *
     * @returns {boolean}
     */
    public isEvent() {
        return false;
    }

}
